# Tezla Syntax and Semantics

To consult the Tezla syntax and semantics, please head to [this](semantics.pdf)
PDF file.
